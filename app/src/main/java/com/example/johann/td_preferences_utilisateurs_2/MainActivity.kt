package com.example.johann.td_preferences_utilisateurs_2

/**
 * Created by johann on 16/10/2017.
 */

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.view.View
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_settings.*
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity() {

    private val nom: String by Preference(this, SettingsActivity.PREF_NOM,
            SettingsActivity.DEFAULT_NOM)

    private val prenom: String by Preference(this, SettingsActivity.PREF_PRENOM,
            SettingsActivity.DEFAULT_PRENOM)

    private val etudiant: String by Preference(this, SettingsActivity.PREF_ETUDIANT,
            SettingsActivity.DEFAULT_PRENOM)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        texte.setOnClickListener { startActivity<SettingsActivity>() }

    }
}
