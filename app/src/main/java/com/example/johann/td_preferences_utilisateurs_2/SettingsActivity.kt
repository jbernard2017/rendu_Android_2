package com.example.johann.td_preferences_utilisateurs_2

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_settings.*
import java.util.prefs.Preferences
import kotlin.reflect.KProperty

class SettingsActivity : AppCompatActivity() {
    companion object {
        val PREF_NOM = "nom"
        val DEFAULT_NOM = "Bernard"
        val PREF_PRENOM = "prenom"
        val DEFAULT_PRENOM = "Johann"
        val PREF_ETUDIANT = "etudiant"
        val DEFAULT_ETUDIANT = false

    }

    private var nom: String by DelegatesExt.preference(this, PREF_NOM, DEFAULT_NOM)
    private var prenom: String by DelegatesExt.preference(this, PREF_PRENOM, DEFAULT_PRENOM)
    private var etudiant: Boolean by DelegatesExt.preference(this, PREF_ETUDIANT, DEFAULT_ETUDIANT)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        nomEditText.text = Editable.Factory.getInstance().newEditable(nom)
        prenomEditText.text = Editable.Factory.getInstance().newEditable(prenom)
        switch1.isChecked = etudiant
    }

    override fun onPause() {
        super.onPause()
        nom=nomEditText.text.toString()
        prenom=prenomEditText.text.toString()
        etudiant=switch1.isChecked
    }
}
